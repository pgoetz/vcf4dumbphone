# vcf4dumbphone

To import your contacts in .vcf format into a dumbphone (e.g. Nokia 3310) you have to simplify your contacts. Every phone number needs to have their own contact. This simple ruby script transforms a regular .vcf file into a simplified one.

## Usage

docker run -it --rm --name vcf4dumbphone -v "$PWD":/usr/src/vcf4dumbphone -w /usr/src/vcf4dumbphone ruby:2.5 ruby vcf4dumbphone.rb [input-file.vcf]
