#!/usr/bin/ruby
# encoding: utf-8

unless ARGV.length == 1
  puts "Usage: ruby vcf4dumbpghone.rb input-file.vcf"
  exit
end

begin_vcard = "BEGIN:VCARD"
end_vcard = "END:VCARD"
version = "VERSION:3.0"
name_prefix = "N:"
name_suffix = ";;;"
tel_prefix = "TEL;VOICE;CELL"

phone_types = { "CELL" => "mobil", "WORK" => "work", "HOME" => "home" }

simplified_vcf = []
current_contact = ""

IO.foreach(ARGV[0], encoding: "utf-8"){ |line|
  if line.start_with?("FN:")
    current_contact = line[3..-2].strip
  end

  if line.start_with?("TEL;")
    phone_type = phone_types[line.match(/type=(CELL|WORK|HOME)/)[1]]

    simplified_vcf.push(begin_vcard)
    simplified_vcf.push(version)
    simplified_vcf.push(name_prefix + current_contact + " " + phone_type + name_suffix)
    simplified_vcf.push(tel_prefix + line[line.index(':')..-2].delete(" "))
    simplified_vcf.push(end_vcard)
    simplified_vcf.push("")
  end
}

File.open("backup.dat", "w") do |file|
  file.puts(simplified_vcf)
end
